import requests
import json
import HTMLParser


class Smmry(object):

    """ Helpful for working with Smmry.com API """

    def __init__(self, key):
        self.api = 'http://api.smmry.com/'
        self.key = key
        self.par_key = '&SM_API_KEY='
        self.par_url = '&SM_URL='
        self.par_length = '&SM_LENGTH='
        self.par_keyword = '&SM_KEYWORD_COUNT='
        self.par_quote_avoid = '&SM_QUOTE_AVOID'
        self.par_with_break = '&SM_WITH_BREAK'

    def __repr__(self):
        return 'Insert Smmry API key as parameter'

    def call_url(self, url, length=4, keyword_count=5, quote_avoid=False, with_break=False):
        link = self.api + self.par_key + self.key + self.par_length + str(length) + self.par_keyword + str(keyword_count)
        if quote_avoid:
            link += self.par_quote_avoid
        if with_break:
            link += self.par_with_break
        return link + self.par_url + url

    def call_dict(self, url, length=4, keyword_count=5, quote_avoid=False, with_break=False):
        link = self.call_url(url, length, keyword_count, quote_avoid, with_break)
        page = requests.get(link)
        page_dict = json.loads(page.content)
        return page_dict

    def clean_response(self, raw_smmry_dict, field='sm_api_content'):
        # convert json response to python dictionary
        raw_data = raw_smmry_dict.get(field)

        # json response needs to be cleaned up for quotes and other escaped characters
        parser = HTMLParser.HTMLParser()

        # clean data with xml_parser
        clean_data = parser.unescape(raw_data)
        clean_data = clean_data.replace('\\"', '"')
        clean_data = clean_data.replace("\\'", "'")
        return clean_data

# key = '9F13AD87AD'
# url = 'http://www.world-science.net/othernews/140403_darkmatter.htm'
# smmry = Smmry(key)
# data = smmry.call_dict(url)
# print smmry.clean_response(data, field='sm_api_title')
