from django.contrib import admin
from core.models import Headlines, UserProfile, Summaries

# Register your models here.
admin.site.register(Headlines)
admin.site.register(Summaries)
admin.site.register(UserProfile)
