from django import forms
from core.models import Headlines, UserProfile, Summaries
from django.contrib.auth.models import User

class HeadlinesForm(forms.ModelForm):
    url = forms.URLField(help_text="Please enter url.")
    comments = forms.CharField(help_text="Please enter any comments.", required=False)
    
    def clean(self):
        cleaned_data = self.cleaned_data
        url = cleaned_data.get('url')

        if not url.startswith('http://'):
            url = 'http://' + url

        cleaned_data['url'] = url
        return cleaned_data

    class Meta:
        # Provide an association between the ModelForm and a model
        model = Headlines
        fields = ('url', 'comments',)

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password',)

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('website', 'picture',)

class SummariesForm(forms.ModelForm):
    url = forms.URLField(help_text="Please enter url.")
    comments = forms.CharField(help_text="Please enter any comments.", required=False)
    
    def clean(self):
        cleaned_data = self.cleaned_data
        url = cleaned_data.get('url')

        if not url.startswith('http://'):
            url = 'http://' + url

        cleaned_data['url'] = url
        return cleaned_data

    class Meta:
        # Provide an association between the ModelForm and a model
        model = Summaries
        fields = ('url', 'comments',)
