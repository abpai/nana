from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Headlines(models.Model):
    url = models.URLField(unique=True)
    comments = models.TextField(blank=True)
    title = models.TextField(blank=True)
    summary = models.TextField(blank=True)
    category = models.TextField(blank=True)
    published = models.DateTimeField(blank=True)    
    added = models.DateTimeField(auto_now_add=True)
    likes = models.IntegerField(default=0, blank=True)
    
    def __unicode__(self):
        return self.title

class Summaries(models.Model):
    user = models.ForeignKey(User)
    url = models.URLField(unique=True)
    comments = models.TextField(blank=True)
    title = models.TextField(blank=True)
    summary = models.TextField(blank=True)
    added = models.DateTimeField(auto_now_add=True)
    
    def __unicode__(self):
        return self.title

class UserProfile(models.Model):
    # This line is required. Links UserProfile to a User model instance.
    user = models.OneToOneField(User)

    # The additional attributes we wish to include.
    website = models.URLField(blank=True)
    picture = models.ImageField(upload_to='profile_images', blank=True)

    # Override the __unicode__() method to return out something meaningful
    def __unicode__(self):
        return self.user.username

class Like(models.Model):
    # need to add related name because Django auto creates a reverse relation from Headlines and User back to Like
    # to reference Like withing Headline's instance, you must go h1.headline_like_by.all() etc.
    user = models.ForeignKey(User, null=True, default=None)
    headline = models.ManyToManyField(Headlines, null=True, default=None, related_name='headline_liked_by')
    date_added = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.user.username


