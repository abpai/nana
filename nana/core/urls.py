from django.conf.urls import patterns, url
from core import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'nana.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    
    url(r'^$', views.index, name='index'),
    url(r'^headlines/(?P<category_name>\w+)', views.headlines, name='headlines'),
    url(r'^my_summaries/', views.my_summaries, name='my_summaries'), 
    url(r'^add/', views.add_url, name='add_url'),
    url(r'^register/', views.register, name='register'),
    url(r'^login/', views.user_login, name='login'),
    url(r'^logout/', views.user_logout, name='logout'),
    url(r'^test/', views.test, name='test'),
    url(r'^search/', views.search, name='search'),
    url(r'^like/', views.like, name='like'),
    url(r'^remove/', views.remove, name='remove'),
    # url(r'^edit/', views.edit, name='edit'),

)

