import random

# from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, StreamingHttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
# from django.views.decorators.csrf import csrf_exempt #to disable csrf protection on view add @csrf_exempt
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.db.models import Q

from endless_pagination.decorators import page_template

from core.models import Headlines, Summaries, Like
from core.forms import UserForm, UserProfileForm, SummariesForm
from core.Smmry import Smmry
# Create your views here.

#helper functions
def clean_url(url):
    if not url.startswith('http://'):
        url = 'http://' + url
    return url



#url views
def test(request):
    pass

def index(request):
    return redirect('/nana/headlines/top')

@page_template('core/more_headlines.html')
def headlines(request, category_name, template='core/base.html', extra_context=None):
    context = RequestContext(request)

# topics
#    : top stories
#   w : world
#   n : us
#   b : business
#   tc : technology
#   e : entertainment
#   s : sports
#   snc : science

    category_options = {'top': ' ',
                        'world': 'w',
                        'us': 'n',
                        'business': 'b',
                        'technology': 'tc',
                        'media': 'e',
                        'sports': 's',
                        'science':'snc'
                        }

    category_key = category_options[category_name]
    if category_name == 'us':
        category_name = category_name.upper()
    else:
        category_name = category_name.capitalize()

    all_headlines = Headlines.objects.filter(category=category_key).order_by('-published')
    
    if extra_context is not None:
        context.update(extra_context)

    if request.user.is_authenticated():
        headlines = []
        for headline in all_headlines:
            headlines.append( (headline,
                               headline.headline_liked_by.filter(user=request.user) ) )

    else:
        headlines = []
        for headline in all_headlines:
            headlines.append( (headline, []))

    
    context_dict = {'headlines': headlines, 'headlines_template': True, 'category': category_name}

    return render_to_response(template, context_dict, context)

@page_template('core/more_user_summaries.html')
@login_required
def my_summaries(request, template='core/base.html', extra_context=None):
    context = RequestContext(request)
    form = SummariesForm()
    
    if request.user.is_authenticated():
        user_summaries = Summaries.objects.filter(user=request.user).order_by('-added')
    else:
        user_summaries = None

    if extra_context is not None:
        context.update(extra_context)

    context_dict = {'form': form, 
                    'user_summaries': user_summaries
                    }

    return render_to_response(template, context_dict, context)


@login_required
def add_url(request):
    context = RequestContext(request)
    user = request.user
    summary_dict = { }
    print request.method
    if request.method == 'POST':
        try:
            # make API call to Smmry
            key = random.choice(['3D3037D0E0', '4DBA02EDB7', '9F13AD87AD'])
            url = clean_url(request.POST['url'])
            smmry = Smmry(key)
            data = smmry.call_dict(url)

            comments = request.POST['comments']
            title = smmry.clean_response(data, field='sm_api_title')
            summary = smmry.clean_response(data, field='sm_api_content')

            # Just for the Ajax response, it displays as recent            

            page = Summaries(user=user, url=url, comments=comments, title=title, summary=summary)
            summary_dict = {'s': page}
            print page
            page.save()
     
        # if error for whatever reason
        except:
            url = clean_url(request.POST['url'])
            comments = request.POST['comments']
            title = url
            summary = 'Not Available'

            page = Summaries(user=user, url=url, comments=comments, title=title, summary=summary)
            summary_dict = {'s': page}
            page.save()

        # for testing only
        return render_to_response('core/latest_add.html', summary_dict, context)        

    else:
        return HttpResponse('There were errors!')

def register(request):
    context = RequestContext(request)
    registered = False
    password_mismatch = False

    if request.method == 'POST':
        if request.POST['password'] != request.POST['password2']:
            password_mismatch = True
            user_form = UserForm()
            profile_form = UserProfileForm()

        else:
            user_form = UserForm(data=request.POST)
            profile_form = UserProfileForm(data=request.POST)

            if user_form.is_valid() and profile_form.is_valid():
                user = user_form.save()
                # Has the password with the set_password method.
                # Once hashed, we can update the user object.
                user.set_password(user.password)
                user.save()

                # Sort out the UserProfile instance.  Set commit=False so that we can set user attribute
                profile = profile_form.save(commit=False)
                profile.user = user

                if 'picture' in request.FILES:
                    profile.picture = request.FILES['picture']

                profile.save()

                registered = True

            else:
                print user_form.errors, profile_form.errors

    else:
        user_form = UserForm()
        profile_form = UserProfileForm()

    return render_to_response(
        'core/register.html', {'user_form': user_form, 'profile_form': profile_form, 'registered': registered, 'password_mismatch': password_mismatch}, 
        context)

def user_login(request):
    context = RequestContext(request)
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        # Tests if provided username and password are correct
        user = authenticate(username=username, password=password)
        # If User object is returned, we have a User object
        if user is not None:
            # If the account is valid and active, we can log the user in.
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/nana/my_summaries/')
            else:
                return HttpResponse('Your account is disabled.')
        else:
            # Bad login details were provided. So we can't log the user in.
            print 'Invalid login details: {0}, {1}'.format(username, password)
            context_dict = {'bad_details': True}
            return render_to_response('core/login.html', context_dict, context)
    else:
        # No context variables to pass to the template system, hence the
        # blank dictionary object...
        return render_to_response('core/login.html', {}, context)

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/nana/')

def search(request):
    context = RequestContext(request)
    searched = request.POST['search']

    headlines = Headlines.objects.filter(Q(summary__icontains=searched) |
                                         Q(title__icontains=searched) |
                                         Q(comments__icontains=searched))

    if request.user.is_authenticated():
        results = True
        my_summaries = Summaries.objects.filter(Q(user=request.user), Q(summary__icontains=searched) |
                                         Q(title__icontains=searched))

        if len(my_summaries) == 0:
            results = False

    else:
        my_summaries = []
        results = False

    context_dict = {'headlines': headlines, 'searched': searched, 'results': results, 'my_summaries': my_summaries}
    return render_to_response('core/search.html', context_dict, context)


def like(request):
    headline_id = None
    user = None
    if request.method == 'GET':
        headline_id = request.GET['headline_id']
        user = request.user

    likes = 0

    if headline_id and user:
        headline = Headlines.objects.get(id=int(headline_id))
        liked_by = Like.objects.get_or_create(user=user)
        liked_by = Like.objects.get(user=user)

        if headline and liked_by:
            #make changes to headline
            likes = headline.likes + 1
            headline.likes = likes
            

            #modify like model to reflect
            liked_by.headline.add(headline)

            #add it to the users My Summaries
            page = Summaries(user=user, url=headline.url, comments=' ', title=headline.title, summary=headline.summary)
            page.save()
            headline.save()

    return HttpResponse(likes)


# def edit(request):
#     return request.user.username

def remove(request):
    user = request.user
    complete = False

    if request.method == 'GET':
        summary_id = request.GET['summary_id']
        if summary_id and user:
            s = Summaries.objects.get(id=summary_id, user=user)
            s.delete()
            complete = True
    return HttpResponse(complete)

