
def populate():
    headlines = get_articles()
    for headline in headlines:
        try:
            s = add_headlines(headline)
            print s

        except:
            print 'failed'
            print headline
            continue

def add_headlines(headline, comments='', likes=0):
    category = headline[0]

    url = headline[1]
    key = random.choice(['enter key', 'enter key', 'enter key'])
    smmry = Smmry(key)
    data = smmry.call_dict(url)
    published = headline[2]
    
    title = smmry.clean_response(data, field='sm_api_title')
    summary = smmry.clean_response(data, field='sm_api_content')
    p = Headlines.objects.get_or_create(url=url, comments='', title=title, summary=summary, category=category, published=published)[0]
    return p

if __name__ == '__main__':
    print "Starting NANA population script..."
    import os
    import random
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nana.settings')
    from core.models import Headlines
    from core.Smmry import Smmry
    from core.Gnews import get_articles
    populate()
