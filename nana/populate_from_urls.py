import os

def populate():
    urls = ['http://time.com/31897/russia-ukraine-crimea-obama-sanctions/',
            'http://www.foxnews.com/world/2014/03/20/australian-pm-says-2-objects-in-search-for-malaysian-jet-may-have-been-found/',
            'http://www.malaysiakini.com/news/257762',
            'http://www.mercurynews.com/nation-world/ci_25386136/wire-plane-debris-hunt-fred-phelps-dead-u',
            'http://www.nytimes.com/2014/03/21/sports/ncaabasketball/oklahoma-falls-in-upset.html',
            'http://investing.businessweek.com/research/stocks/news/article.asp?docKey=600-201403202042PR_NEWS_USPRX____LA88089-1&params=timestamp%7C%7C03/20/2014%208:42%20PM%20ET%7C%7Cheadline%7C%7CZions%20Bancorporation%20Will%20Resubmit%20Capital%20Plan%7C%7CdocSource%7C%7CPR%20Newswire%7C%7Cprovider%7C%7CACQUIREMEDIA%7C%7Cbridgesymbol%7C%7CUS;ZION&ticker=ZION',
            'http://www.nydailynews.com/entertainment/music-arts/kurt-cobain-death-scene-photos-examined-seattle-police-article-1.1728415',
            'http://www.reuters.com/article/2014/03/21/us-usa-execution-florida-idUSBREA2J2EY20140321',
            'http://ftw.usatoday.com/2014/03/ncaa-billion-dollar-ohio-state-upset/',
            'http://www.bloomberg.com/news/2014-03-20/low-inflation-gives-yellen-room-to-retreat-from-rate-outlook.html',
            'http://www.myfoxal.com/story/25034396/irs-warns-taxpayers-about-phone-scammers-posing-as-irs-agents',
            'http://venturebeat.com/2014/03/20/netflix-is-still-pro-net-neutrality-even-after-its-traffic-deal-with-comcast/']
    
    i = 0
    for url in urls:
        count = 'Headlines ' + str(i)
        add_headlines(url, comments=count)
        i += 1

    for news in Headlines.objects.all():
        print news.title


def add_headlines(url, comments='', category='top', likes=0, published=''):
    key = 'enter key'
    smmry = Smmry(key)
    data = smmry.call_dict(url)

    title = smmry.clean_response(data, field='sm_api_title')
    summary = smmry.clean_response(data, field='sm_api_content')

    p = Headlines.objects.get_or_create(url=url, comments=comments, title=title, summary=summary, likes=likes, category=category, published=published)[0]
    return p

if __name__ == '__main__':
    print "Starting NANA population script..."
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nana.settings')
    from core.models import Headlines
    from core.Smmry import Smmry
    populate()
