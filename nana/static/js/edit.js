$(document).on("click", "#remove", function (ev) {
    var clicked = $(this);
    console.log(clicked);
    $.ajax({
        url: '/nana/remove/',
        data: {'summary_id': clicked.attr( 'data-sumid' )},
        beforeSend: function () {
            clicked.parents(".well").remove();
        },
        complete: function(){
            console.log('remove successful');
        },
    });
    ev.preventDefault();
});
