// $(document).on("click", "#summarize", function (ev) {
    // $.post( "/nana/add/", {url: submitted_url, comments: submitted_comments}, function( data ) {
    //     $( "#summary" ).html( data );
    // });

var submitted_url = $( "input[name='url']" );
var submitted_comments = $( "textarea[name='comments']" );
var spinner = $('#spinner');
var frm = $('#url_submit');

frm.submit(function (ev) {
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        beforeSend: function () {
            spinner.show();
            console.log('Sent');
            submitted_url.val("");
            submitted_comments.val("");
        },
        complete: function () {
            spinner.hide();
            console.log('Complete');

        },
        success: function (data) {
            $("#new_summary").prepend(data);
        }
    });
    ev.preventDefault();
});