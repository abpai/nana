// $(document).on("click", "#summarize", function (ev) {
    // $.post( "/nana/add/", {url: submitted_url, comments: submitted_comments}, function( data ) {
    //     $( "#summary" ).html( data );
    // });

// var total_likes = $( '#total_likes' );
// var like_container = $( '#like_container' );
// var like_button = $( '#like' );
// var headline_id = like_button.attr( 'data-headid' );

$(document).on("click", "#like", function (ev) {
    var clicked = $(this);
    $.ajax({
        url: '/nana/like/',
        data: {'headline_id': clicked.attr( 'data-headid' )},
        beforeSend: function () {
            text = clicked.parent().prevAll("#total_likes:first").text();
            var updated_likes = parseInt(text.substring(0, text.length -6), 10) + 1;
            clicked.parent().prevAll("#total_likes:first").text(updated_likes.toString()+' likes');
            clicked.replaceWith("<button style='margin-left: 4px;' class=\"reveal btn btn-info disabled btn-xs\" type=\"button\"><em>Liked</em></button>");
        },
        complete: function(){
            console.log('like complete');
        },
    });
    ev.preventDefault();
});